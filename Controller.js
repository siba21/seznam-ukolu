class Controller {
    constructor(model, view) {
      this.model = model
      this.view = view
  
      // Počáteční zobrazení seznami úkolů
      this.zmenaSeznamuUkolu(this.model.ukoly)

      this.view.bindPridejUkol(this.resPridejUkol)
      this.view.bindSmazUkol(this.resSmazUkol)
      this.view.bindSplnUkol(this.resSplnUkol)
      this.view.bindEditUkolu(this.resEditUkolu)
      this.view.bindFiltruj(this.resFiltr)
      this.model.bindZmenaSeznamuUkolu(this.zmenaSeznamuUkolu)
     
  }

  zmenaSeznamuUkolu = ukoly => {
    this.view.zobrazUkoly(ukoly)
  }

  resPridejUkol = textUkolu => {
    this.model.pridejUkol(textUkolu)
  }
  
  resEditUkolu = (id, novyText) => {
    this.model.editUkolu(id, novyText)
  }
  
  resSmazUkol = id => {
    this.model.smazUkol(id)
  }
  
  resSplnUkol = id => {
    this.model.splnUkol(id)
  }
  
  resDD = arr => {
    this.model.sortSeznamu(arr)
  }
  
  resFiltr = val => {
    this.model.filtrUkolu(val)
  }

}