class Model {

    constructor() {
      // Model je array dat, předvyplněná nějaká data
      this.ukoly = JSON.parse(localStorage.getItem('ukoly')) || []
    }
  
    // Přidá nový úkol
    pridejUkol(textUkolu) {
        let maxID = 0
        if(this.ukoly.length > 0) {           
            this.ukoly.forEach((val) => {
                if(maxID < Number(val.id)) {
                    maxID = Number(val.id)
                }
            })
            //console.log(maxID)
        }
      const ukol = {
        id: this.ukoly.length > 0 ? maxID + 1 : 1,
        text: textUkolu,
        complete: false,
      }
  
      this.ukoly.push(ukol)

      this.ulozit(this.ukoly)
      //console.log("Vytvořen úkol s ID: " + ukol.id);
      //console.log(app.model.ukoly)
    }
  
    // Projede úkoly a upraví text u úkolu se shodným ID
    editUkolu(id, novyText) {
      this.ukoly = this.ukoly.map(ukol =>
        ukol.id === id ? { id: ukol.id, text: novyText, complete: ukol.complete } : ukol
      )

      this.ulozit(this.ukoly)
      //console.log("Text úkolu s ID: " + id + " upraven");
    }
  
    // Vyfiltruje úkol se shodnym ID
    smazUkol(id) {
      this.ukoly = this.ukoly.filter(ukol => ukol.id !== id)

      this.ulozit(this.ukoly)
      //console.log("Úkol s ID: " + id + " smazán");
    }
    
    vratUkol(id) {
        return this.ukoly.filter(ukol => ukol.id === id)[0] 
    }
  
    // Přepnutí booleanu complete u úkolu s daným ID
    splnUkol(id) {
      this.ukoly = this.ukoly.map(ukol =>
        ukol.id === id ? { id: ukol.id, text: ukol.text, complete: !ukol.complete } : ukol
      )

      this.ulozit(this.ukoly)
      //console.log("Boolean complete u úkolu s ID: " + id + " přepnut");
    }
    
    sortSeznamu(arr) {
        var ukolySort = []  
        arr.forEach( (val, index) => {
            //console.log(val.id, index)
            ukolySort[index] = this.vratUkol(Number(val.id))
        })
        
        this.ulozit(ukolySort)
        this.ukoly = ukolySort
        
        //console.log(ukolySort)
        //console.log(this.ukoly) 
    }
    
    filtrUkolu(stav) {
        if(stav === "all") {
            this.ukoly = JSON.parse(localStorage.getItem('ukoly')) || []
            this.zmenaSeznamuUkolu(this.ukoly)    
        } else if(stav === "done") {
            var ukolyFil= this.ukoly.filter(ukol => ukol.complete === true)
            this.zmenaSeznamuUkolu(ukolyFil)
        } else if(stav === "todo") {
            var ukolyFil= this.ukoly.filter(ukol => ukol.complete === false)
            this.zmenaSeznamuUkolu(ukolyFil)
        }    
    }

    bindZmenaSeznamuUkolu(callback) {
      this.zmenaSeznamuUkolu = callback
    }

    // Uložení dat do LocalStorage, zavolání zobrazUkoly()
    ulozit(ukoly) {
      this.zmenaSeznamuUkolu(ukoly)
      localStorage.setItem('ukoly', JSON.stringify(ukoly))
    }

  }