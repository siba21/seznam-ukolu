class View {
  constructor() {
     // Root element
     this.app = this.getElement('#root')

     // Název aplikace
     this.název = this.createElement('h1')
     this.název.textContent = 'Seznam úkolů'

     this.název2 = this.createElement('h2')
     this.název2.textContent = ' > - - - - - - - - - - - - - - - - - - - - <'
 
     // Formulář, Input pole typu text s tlačítkem odeslat
     this.form = this.createElement('form')
 
     this.pole = this.createElement('input')
     this.pole.type = 'text'
     this.pole.placeholder = 'Přidat úkol'
     this.pole.name = 'ukol'
 
     this.tlačítkoOdeslat = this.createElement('button')
     this.tlačítkoOdeslat.textContent = 'Odeslat'
 
     // Zobrazení seznamu úkolů
     this.seznamUkolu = this.createElement('ul', 'container', 'sortable')
     
     //Filtrování
     this.filtr = this.createElement('select', '', 'filtr')
     this.op1 =  this.createElement('option')
     this.op1.text = 'Vše'
     this.op1.value = 'all'
     
     this.op2 =  this.createElement('option')
     this.op2.text = 'Splněné'
     this.op2.value = 'done'
     
     this.op3 =  this.createElement('option')
     this.op3.text = 'Nesplněné'
     this.op3.value = 'todo'
     
     this.filtr.append(this.op1, this.op2, this.op3)
 
     // Připojení input pole a odeslat k formuláři
     this.form.append(this.pole, this.tlačítkoOdeslat)
 
     // Připojení názvu, formuláže a seznamu úkolů k rootu aplikace
     this.app.append(this.název, this.název2, this.form, this.filtr, this.seznamUkolu)

     //console.log("View_constructor_fin")

     this.dočasnýTextÚkolu
     this.dočasnýText()
      

   }
  

  // Vytvoření elementu s možností CSS třídy
  createElement(tag, className, idName) {
    const element = document.createElement(tag)
    if (className) element.classList.add(className)
    if (idName) element.setAttribute("id", idName)

    //console.log("Vytvořen element " + tag)

    return element
  }

  // Získání elementu z DOM
  getElement(selector) {
    const element = document.querySelector(selector)

    //console.log("Zíslán element se selectorem " + selector)

    return element
  }

  get textUkolu() {
    return this.pole.value
  }
  
  resetInput() {
    this.pole.value = ''
  }

  zobrazUkoly(ukoly) {
    var i=0
    // 1. Smažeme všechny úkoly - vyčistíme zobrazený seznam
    while (this.seznamUkolu.firstChild) {
      this.seznamUkolu.removeChild(this.seznamUkolu.firstChild)
    }

    //console.log("Zobrazení seznamu úkolů vyčištěno")

    // Výchozí zpráva - zobrazí se, když nejsou žádné poznámky
    if (ukoly.length === 0) {
      const p = this.createElement('p')
      p.textContent = 'Nic na práci! Přidat úkol?'
      this.seznamUkolu.append(p)
      //console.log("Zobrazena defaultní zpráva")
    } else {
      // Vytvoří li elementy jednotlivých úkolů
      
      ukoly.forEach(ukol => {
        const li = this.createElement('li', "", i)
        li.id = ukol.id
        i++

        // Checkboxy jednotlivých úkolů
        const checkbox = this.createElement('input')
        checkbox.type = 'checkbox'
        checkbox.checked = ukol.complete

        // Jednotlivé položky úkolů v editovatelných spanech
        const span = this.createElement('span', 'editable')
        span.contentEditable = true

        // Přeškrtnutí splněného úkolu
        if (ukol.complete) {
          const skrtnuti = this.createElement('s')
          skrtnuti.textContent = ukol.text
          span.append(skrtnuti)
        } else {
          span.textContent = ukol.text
        }

        // Tlačítko Smazat u úkolů
        const tlacitkoSmazat = this.createElement('button', 'smazat')
        tlacitkoSmazat.textContent = 'Smazat'

        // Připojení jednotlivých funkcí
        li.append(checkbox, span, tlacitkoSmazat)

        // Připojení jednotlivých li k seznamu úkolů
        this.seznamUkolu.append(li)
        //console.log("Zobrazen seznam úkolů")
      })
    }

  }

  bindPridejUkol(handler) {
    this.form.addEventListener('submit', event => {
      event.preventDefault()
  
      if (this.textUkolu) {
        handler(this.textUkolu)
        this.resetInput()
      }
    })
  }
  
  bindSmazUkol(handler) {
    this.seznamUkolu.addEventListener('click', event => {
      if (event.target.className === 'smazat') {
        const id = parseInt(event.target.parentElement.id)
  
        handler(id)
      }
    })
  }
  
  bindSplnUkol(handler) {
    this.seznamUkolu.addEventListener('change', event => {
      if (event.target.type === 'checkbox') {
        const id = parseInt(event.target.parentElement.id)
  
        handler(id)
      }
    })
  }
  
    bindFiltruj(handler) {
    this.filtr.addEventListener('change', event => {
      const val = this.filtr.options[this.filtr.selectedIndex].value
      
      handler(val)
    })
  }

  // Úprava dočasného stavu textu
  dočasnýText() {
    this.seznamUkolu.addEventListener('input', event => {
      if (event.target.className === 'editable') {
        this.dočasnýTextÚkolu = event.target.innerText
      }
    })
  }

  // Poslání nového textu modelu
  bindEditUkolu(handler) {
    this.seznamUkolu.addEventListener('focusout', event => {
      if (this.dočasnýTextÚkolu) {
        const id = parseInt(event.target.parentElement.id)

        handler(id, this.dočasnýTextÚkolu)
        this.dočasnýTextÚkolu = ''
      }
    })
  }

}